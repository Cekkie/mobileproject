package com.cek.simon.mobileproject;

import java.util.ArrayList;
import java.util.List;
import com.cek.simon.mobileproject.Task;
/**
 * Created by simon on 11/22/2015.
 */
public class TaskList {

    private int id;
    private String name;
    private List<Task> tasks;

    public TaskList(){

    }

    public TaskList(int id, String name){
        this.id = id;
        this.name = name;
        this.tasks = new ArrayList<Task>();
    }

    private void setName(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }

    private void setId(int i){
        this.id = id;
    }

    public int getId(){
        return id;
    }

    public void addTask(Task t){
        tasks.add(t);
    }

    public void removeTask(int id){
        for (Task t: tasks) {
            if(t.getId() == id){
                tasks.remove(t);
            }
        }
    }

    public Task getTask(int id){
        for (Task t: tasks) {
            if(t.getId() == id){
                return t;
            }
        }
        return null;
    }

}
