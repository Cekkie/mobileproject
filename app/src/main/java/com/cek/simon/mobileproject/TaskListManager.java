package com.cek.simon.mobileproject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by simon on 11/22/2015.
 */
public class TaskListManager {


    private List<com.cek.simon.mobileproject.TaskList> manager;

    public TaskListManager(){
        manager = new ArrayList<>();
    }

    public void addTaskList(TaskList list){
        manager.add(list);
    }

    public void removeTaskList(int id){
        for(TaskList l : manager){
            if(l.getId() == id){
                manager.remove(l);
            }
        }
    }


}
