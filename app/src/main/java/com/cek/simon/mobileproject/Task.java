package com.cek.simon.mobileproject;

/**
 * Created by simon on 11/22/2015.
 */
public class Task {

    private int id;
    private String description;
    private boolean isChecked;

    public Task(){

    }

    public Task(int id, String description){
        this.id = id;
        this.description = description;
        this.isChecked = false;
    }

    private void setId(int id){
        this.id = id;
    }

    public int getId(){
        return id;
    }

    private void setDescription(String description){
        this.description = description;
    }

    public String getDescription(){
        return description;
    }

    private void setChecked(boolean checked){
        this.isChecked = checked;
    }

    public boolean getisChecked(){
        return isChecked;
    }


}
